"Autocmds
"Remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Auto-resize splits when Vim gets resized.
autocmd VimResized * wincmd =

" Enable spellcheck for txt,markdown files
autocmd BufRead,BufNewFile *.md,*.txt setlocal spell

" Terminal settings
augroup term_settings
  autocmd!
  " Do not use number and relative number for terminal inside nvim
  autocmd TermOpen * setlocal norelativenumber nonumber
  " Go to insert mode by default to start typing command
  " deactivated line due to error with coc-split-term on spawning the term
  " autocmd TermOpen * startinsert
augroup END
"
" Display a message when the current file is not in utf-8 format.
" Note that we need to use `unsilent` command here because of this issue:
" https://github.com/vim/vim/issues/4379
augroup non_utf8_file_warn
  autocmd!
  autocmd BufRead * if &fileencoding != 'utf-8' | unsilent echomsg 'File not in UTF-8 format!' | endif
augroup END

" When opening a CSV file, open it as a log-filel.
" Large CSV's get rendered and Nvim is very slow at that.
autocmd BufEnter *.csv set ft=log

" Mappings to make Vim more friendly towards presenting slides.
" using .vs (vimslide as a filetype)
autocmd BufNewFile,BufRead *.vs call SetVimPresentationMode()

function! SetVimPresentationMode()
  nnoremap <buffer> <Right> :n<CR>
  nnoremap <buffer> <Left> :N<CR>
endfunction

function! ToggleCalendar()
  execute ":Calendar"
  if exists("g:calendar_open")
    if g:calendar_open == 1
      execute "q"
      unlet g:calendar_open
    else
      g:calendar_open = 1
    end
  else
    let g:calendar_open = 1
  end
endfunction
