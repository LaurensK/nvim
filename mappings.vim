" Define leader key
let g:mapleader = "\<Space>"
nnoremap <Space> <Nop>

" Better indenting
vnoremap < <gv
vnoremap > >gv

" Better window navigation
imap <C-h> <C-w>h
imap <C-j> <C-w>j
imap <C-k> <C-w>k
imap <C-l> <C-w>l

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

imap <C-left> <C-w>h
imap <C-down> <C-w>j
imap <C-up> <C-w>k
imap <C-right> <C-w>l

nnoremap <C-left> <C-w>h
nnoremap <C-down> <C-w>j
nnoremap <C-up> <C-w>k
nnoremap <C-right> <C-w>l

" Terminal window navigation
tnoremap <C-h> <C-\><C-N><C-w>h
tnoremap <C-j> <C-\><C-N><C-w>j
tnoremap <C-k> <C-\><C-N><C-w>k
tnoremap <C-l> <C-\><C-N><C-w>l
inoremap <C-h> <C-\><C-N><C-w>h
inoremap <C-j> <C-\><C-N><C-w>j
inoremap <C-k> <C-\><C-N><C-w>k
inoremap <C-l> <C-\><C-N><C-w>l
tnoremap <Esc> <C-\><C-n>

" Use ctrl + alt + hjkl to resize windows
nnoremap <silent> <C-M-J>    :resize -2<CR>
nnoremap <silent> <C-M-K>    :resize +2<CR>
nnoremap <silent> <C-M-H>    :vertical resize -2<CR>
nnoremap <silent> <C-M-L>    :vertical resize +2<CR>

nnoremap <silent> <C-M-down>  :resize -2<CR>
nnoremap <silent> <C-M-up>    :resize +2<CR>
nnoremap <silent> <C-M-left>  :vertical resize -2<CR>
nnoremap <silent> <C-M-right> :vertical resize +2<CR>

"This unsets the "last search pattern" register by hitting return
nnoremap <CR> :noh<CR>

" Tab shortcuts
nnoremap <leader>p :tabp<CR>
nnoremap <leader>n :tabn<CR>

" Move a line of text using ALT+[jk]
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

nmap <M-Down> mz:m+<cr>`z
nmap <M-Up> mz:m-2<cr>`z
vmap <M-Down> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-Up> :m'<-2<cr>`>my`<mzgv`yo`z

" Enable spell checking, s for spell check off
map <leader>se :setlocal spell! spelllang=en_us<CR>
map <leader>sn :setlocal spell! spelllang=nl<CR>
map <leader>s :setlocal spell! spelllang<CR>

" Enable Disable Auto Indent
map <leader>i :setlocal autoindent<CR>
map <leader>I :setlocal noautoindent<CR>

" Enable and disable auto comment
"disbabled: <leader>fc = coc-fix-current
"map <leader>fc :setlocal formatoptions-=cro<CR>
"map <leader>C :setlocal formatoptions=cro<CR>

" Quick save/exit
" save buffers
nmap <leader>w :wa<cr>
vmap <leader>w :wa<cr>

" quit current buffer
nmap <leader>q :q<cr>
vmap <leader>q :q<cr>

" quit Nvim without save!
nmap <leader>Q :qa!<cr>
vmap <leader>Q :qa!<cr>

" Quite often I type a capital Q or W for saving. Therefore just map it to the
" right command
command! -nargs=0 Wa :wa
command! -nargs=0 Qa :qa

command! Xs :mks! | :xa "save the session, save modified files, and exit
nnoremap <leader>ss :mks!<CR> "update saved session

" Other easy mappings
" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
map Y y$

"Set a date!
" Type F3 to write date. Added on 2019-07-01 Mon 03:23 PM
nmap <leader>d i<C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><Esc>
