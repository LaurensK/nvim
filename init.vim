"0. Installation:
" Neovim: Arch Official Repository package
" neovim-plug: Arch Official Repository package

"1. load plugins
source $HOME/.config/nvim/plugins.vim

"2. Plugin settings
source $HOME/.config/nvim/lightline.vim
source $HOME/.config/nvim/fzf.vim
source $HOME/.config/nvim/coc.vim
source $HOME/.config/nvim/mkpv.vim
source $HOME/.config/nvim/vimwiki.vim
source $HOME/.config/nvim/hexokinase.vim

"3. general settings
source $HOME/.config/nvim/settings.vim

"4. autocommands
source $HOME/.config/nvim/autocmds_and_functions.vim

"5. general mappings
source $HOME/.config/nvim/mappings.vim
source $HOME/.config/nvim/coc-extension-mappings.vim

"6. load function key mappings
source $HOME/.config/nvim/function-keys.vim

"7. ginit.vim is automatically loaded if a nvim-gui is started.

"8. load additional project specific settings
silent! source .vimlocal
