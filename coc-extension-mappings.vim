" coc-restclient
augroup restclient
  autocmd!

  autocmd BufRead,BufNewFile,BufEnter *.http set filetype=http

  " Disable CoC suggestions due to TAB key mappings
  autocmd FileType http let b:coc_suggest_disable = 1

  " additional nice mappings
  autocmd FileType http noremap <leader>r :CocCommand rest-client.request<cr>

augroup end

" coc-explorer
nnoremap <silent><nowait> <space>e :<C-u>CocCommand explorer --toggle<cr>

" coc-split-term
noremap <leader>t :CocCommand split-term.Toggle<cr>

" coc-swagger short command
command -nargs=0 Swagger :CocCommand swagger.render
