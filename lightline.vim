let g:lightline = {
      \ 'colorscheme': 'material_vim',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'cocstatus', 'filename', 'modified', 'gitbranch', 'readonly' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead'
      \ },
      \ }

autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()

" Always display the status line
set laststatus=2

" Do not show duplicate mode info
set noshowmode
