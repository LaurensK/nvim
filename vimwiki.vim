let g:vimwiki_list = [{'syntax': 'markdown'}]
let g:vimwiki_global_ext = 0

augroup vimwikigroup
  autocmd!

  " Needs tuning due to markdown code blocks
  autocmd FileType vimwiki setlocal shiftwidth=6 tabstop=6 noexpandtab

  autocmd BufRead,BufNewFile *.wiki set filetype=vimwiki

  " automatically update links on read diary
  autocmd BufRead,BufNewFile diary.wiki VimwikiDiaryGenerateLinks

  " Disable CoC suggestions due to TAB key mappings
  autocmd FileType vimwiki let b:coc_suggest_disable = 1

  " additional nice mappings
  autocmd FileType vimwiki map <leader>wc :call ToggleCalendar()

augroup end

