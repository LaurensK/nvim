"Default encoding and file format for about everything
set fileencoding=utf-8
set fileformats=unix,dos

"Clipboard
set clipboard=unnamedplus           	"Copy paste between vim and everything else

"Warnings
set noerrorbells			                 "no sounds on error

"Session
set history=200				                "Keep 200 commands in history
set hidden				                    "keep buffers on leave
set nobackup				                  "turn off backup and swap
set noswapfile			                	"turn off backup and swap
set undodir=~/.cache/nvim/undodir 	  "use this location for persistent undos
set undofile				                  "activate persistent undo

"Syntax highlighting
syntax on				                      "activate default syntax highlighting
filetype plugin indent on			              "activate filetype detection

"Mouse
set mouse=a				                    "mouse available for all modes

"Buffer filesystem
set autoread                          " Automatically re-read file if a change was detected outside of vim

"UI
set termguicolors                     "enable terminal true color usage
set nowrap                            "Don't wrap lines
set scrolloff=5                       "keep lines visible when scrolling
set cmdheight=2                       "More space for displaying messages
set colorcolumn=100                    "Highlight col 100 for breaking lines
highlight ColorColumn ctermbg=0 guibg=lightgrey
set showtabline=2                     "always show tabline
set splitbelow splitright             "default split settings
set title                             "set window title
set number relativenumber             "show linenumer current line and relative numbers for other lines
set cursorline                        "Cursor

"Theme
let g:material_theme_style = 'ocean'
let g:material_terminal_italics = 1
colorscheme material

"Search in current buffer
set ignorecase smartcase              "Ignore case in general, but become case-sensitive when uppercase is present

"highlight search results while typing
augroup vimrc-incsearch-highlight
	autocmd!
	autocmd CmdlineEnter /,\? :set hlsearch
	autocmd CmdlineLeave /,\? :set nohlsearch
augroup END

" General tab settings
set tabstop=4                         " number of visual spaces per TAB
set softtabstop=4                     " number of spaces in tab when editing
set shiftwidth=2                      " number of spaces to use for autoindent
set expandtab                         " expand tab to spaces so that tabs are spaces
set shiftround                        " indent in multiples of shiftwidth

set smarttab                          "Makes tabbing smarter will realize you have 2 vs 4
set smartindent                       "Makes indenting smart

" Set matching pairs of characters and highlight matching brackets
set matchpairs+=<:>,「:」,『:』,【:】,“:”,‘:’,《:》

" Formatting
set linebreak		"break line of txt on default chars like a '.'
set showbreak=↪

" Keys and command interaction
set backspace=indent,eol,start          " Allow backspacing over autoindent, line breaks and start of insert action
" turned off, does not work nicely with <leader>. combo's
" set notimeout ttimeout ttimeoutlen=200  " Quickly time out on keycodes, but never time out on mappings

" Wildmode - turned off because mostly handles by CoC
" set wildmode
" List all matches and complete till longest common string
"set wildmode=list:longest

"Disable Netrw
let loaded_netrwPlugin = 1
