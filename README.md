My personal 'Neovim init.vim'-repository.

This is an extended [Neovim](https://neovim.io/) configuration aimed at many
different tasks. This is also a growing organism. It may contains an occasional
error.  It is heavily based on configurations published by many other
Vim/Neovim enthousiasts. Thank you for you inspiration.  This configuration is
based on a 'init.vim' file sourcing a number of sorted files each configuring a
specific part(generic settings, mappings, autocommands, plugins, theme etc.).
I keep the number of plugins limited and try to rely on
[CoC](https://github.com/neoclide/coc.nvim) extensions where possible or
convenient.
