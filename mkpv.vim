let g:mkdp_auto_start = 0
let g:mkdp_auto_close = 1

"only refresh preview on save
let g:mkdp_refresh_slow = 1

"only preview markdown
let g:mkdp_command_for_global = 0

"only preview for localhost
let g:mkdp_open_to_the_world = 0

function g:Open_browser(url)
  let url = a:url
  if url != ""
    echo url
    silent exec "!qutebrowser --target=window '"url"'"
  else
    echo "No URL."
  endif
endfunction

let g:mkdp_browserfunc = 'g:Open_browser'

" options for markdown render
" mkit: markdown-it options for render
" katex: katex options for math
" uml: markdown-it-plantuml options
" maid: mermaid options
" disable_sync_scroll: if disable sync scroll, default 0
" sync_scroll_type: 'middle', 'top' or 'relative', default value is 'middle'
"   middle: mean the cursor position alway show at the middle of the preview page
"   top: mean the vim top viewport alway show at the top of the preview page
"   relative: mean the cursor position alway show at the relative positon of the preview page
" hide_yaml_meta: if hide yaml metadata, default is 1
" sequence_diagrams: js-sequence-diagrams options
" content_editable: if enable content editable for preview page, default: v:false
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 1,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {},
    \ 'content_editable': v:false
    \ }

let g:mkdp_markdown_css = ''
let g:mkdp_highlight_css = ''
let g:mkdp_page_title = '「${name}」'
