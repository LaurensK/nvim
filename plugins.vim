"Package manager: 'junegunn/vim-plug'
"Installed from AUR package:
"http://aur.archlinux.org/packages/neovim-plug

call plug#begin('~/.config/nvim/autoload/plugged')

  "Code Editing plugins
  Plug 'tpope/vim-commentary'

  "Syntax plugins
  Plug 'sheerun/vim-polyglot'

  "Git plugins
  Plug 'tpope/vim-fugitive'

  "Statusline
  Plug 'itchyny/lightline.vim'

  "Intellisense/autocomplete/tags
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'majutsushi/tagbar'

  "Development, running and testing
  Plug 'vim-test/vim-test'

  "Show colors
  Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }

  " FZF - find files
  Plug 'junegunn/fzf.vim'

  "Persistent undo usage
  Plug 'mbbill/undotree'

  "Markdown preview
  Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }

  " Themes
  Plug 'joshdick/onedark.vim'
  Plug 'cocopon/iceberg.vim/'
  Plug 'kaicataldo/material.vim', { 'branch': 'main' }

  " Vimwiki
  Plug 'mattn/calendar-vim'
  Plug 'vimwiki/vimwiki'

  " Goyo for presentations and distraction free reading
  Plug 'junegunn/goyo.vim'

call plug#end()
