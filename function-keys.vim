" Mappings for Function keys
nnoremap <silent><nowait> <F2>  :<C-u>CocCommand explorer --toggle<cr>
nnoremap <silent><nowait> <F3>  :TagbarToggle<cr>
nnoremap <silent><nowait> <F4>  :UndotreeToggle<cr>
nnoremap <silent><nowait> <F5>  :Files<cr>
nnoremap <silent><nowait> <F6>  :Rg<cr>
nnoremap <silent><nowait> <F7>  :Lines<cr>
nnoremap <silent><nowait> <F8>  :Windows<cr>
nnoremap <silent><nowait> <F9>  :G<cr>
nnoremap <silent><nowait> <F10>  :call ToggleCalendar()<cr>
nnoremap <silent><nowait> <F11>  :MarkdownPreview<cr>
nnoremap <silent><nowait> <F12>  :tabnew \| :terminal lazygit<cr>i
